package org.idonotthinkthis.works.machinelearning.logisticregression;

import org.idonotthinkthis.works.machinelearning.DataSet;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.function.BiFunction;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.idonotthinkthis.works.machinelearning.logisticregression.DataUtils.*;

public class LogisticCostFunctionTest {
    private static final BiFunction<double[], double[], Double> SIMPLE_HYPOTHESIS =
            (theta, data) -> theta[0] + theta[1] * data[0];

    private static final BiFunction<double[], double[], Double> COMPLEX_HYPOTHESIS =
            (theta, data) -> theta[0] + theta[1] * data[0] + theta[2] * data[1] + theta[3] * data[2];

    @ParameterizedTest
    @MethodSource("provideThetaForSingleDataEntry")
    public void cost_singleDataEntry_calculatesExpectedResult(double[] theta, double result) {
        LogisticCostFunction costFunction = new LogisticCostFunction(SIMPLE_HYPOTHESIS);
        DataSet dataSet = new DataSet(singleValueData(1.1), results(0));

        var cost = costFunction.cost(theta, dataSet);

        assertThat(cost).isEqualTo(result);
    }

    private static Stream<Arguments> provideThetaForSingleDataEntry() {
        return Stream.of(
                Arguments.of(theta(-1, -1), 0.11551952317975495),
                Arguments.of(theta(-0.5, -0.5), 0.3000584796176432),
                Arguments.of(theta(0, 0), 0.6931471805599453),
                Arguments.of(theta(0.5, 0.5), 1.3500584796176431),
                Arguments.of(theta(1, 1), 2.215519523179755),
                Arguments.of(theta(5, 5), 10.500027536066415)
        );
    }

    @ParameterizedTest
    @MethodSource("provideThetaForMultipleDataEntry")
    public void cost_multipleDataEntry_calculatesExpectedResult(double[] theta, double result) {
        LogisticCostFunction costFunction = new LogisticCostFunction(SIMPLE_HYPOTHESIS);
        DataSet dataSet = new DataSet(singleValueData(-3, -1.1, 0, 1.1, 4), results(0, 0, 1, 1, 1));

        var cost = costFunction.cost(theta, dataSet);

        assertThat(cost).isEqualTo(result);
    }

    private static Stream<Arguments> provideThetaForMultipleDataEntry() {
        return Stream.of(
                Arguments.of(theta(-1, -1), 2.2813642460607277),
                Arguments.of(theta(-0.5, -0.5), 1.386949306724362),
                Arguments.of(theta(0, 0), 0.6931471805599454),
                Arguments.of(theta(0.5, 0.5), 0.36694930672436166),
                Arguments.of(theta(1, 1), 0.24136424606072784),
                Arguments.of(theta(5, 5), 0.09617305353051164)
        );
    }

    @ParameterizedTest
    @MethodSource("provideThetaForComplexHypothesis")
    public void cost_complexHypothesis_calculatesExpectedResult(double[] theta, double result) {
        LogisticCostFunction costFunction = new LogisticCostFunction(COMPLEX_HYPOTHESIS);
        DataSet dataSet = new DataSet(
                complexData(array(-3, -3, -0.1), array(-1.1, -1, -1.5), array(0, 0, 0),
                        array(1.1, 0.5, 1.5), array(4, 3, 4)),
                results(0,0, 1, 1, 1));

        var cost = costFunction.cost(theta, dataSet);

        assertThat(cost).isEqualTo(result);
    }

    private static Stream<Arguments> provideThetaForComplexHypothesis() {
        return Stream.of(
                Arguments.of(theta(-1, -1, -1, -1), 5.041485521506807),
                Arguments.of(theta(-0.5, -0.5, -0.5, -0.5), 2.662768360186936),
                Arguments.of(theta(0, 0, 0, 0), 0.6931471805599454),
                Arguments.of(theta(0.5, 0.5, 0.5, 0.5), 0.1827683601869362),
                Arguments.of(theta(1, 1, 1.1, 0.8), 0.08515233851750159),
                Arguments.of(theta(3, 4, 5, 6), 0.009717512567788928)
        );
    }
}
