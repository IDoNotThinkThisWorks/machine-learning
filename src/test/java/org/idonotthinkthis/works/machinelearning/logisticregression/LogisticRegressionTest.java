package org.idonotthinkthis.works.machinelearning.logisticregression;

import org.idonotthinkthis.works.machinelearning.DataSet;
import org.idonotthinkthis.works.machinelearning.utils.RandomProvider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.idonotthinkthis.works.machinelearning.logisticregression.DataUtils.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;

public class LogisticRegressionTest {
    private RandomProvider randomProvider;
    private LogisticRegressionConfig.Builder config;

    private LogisticRegression logisticRegression;

    @BeforeEach
    public void setUp() {
        randomProvider = mock(RandomProvider.class);
        doAnswer((inv) -> 1).when(randomProvider).nextInt(anyInt());

        config = prepareConfig();
    }

    @ParameterizedTest
    @MethodSource("provideTheta")
    public void learnTheta_thetaChanges_learnsExpectedTheta(double[] theta, double[] expectedTheta) {
        logisticRegression = new LogisticRegression(config.withInitialTheta(theta).build(), randomProvider);

        double[] newTheta = logisticRegression.learnTheta(prepareDataSet());

        assertThat(newTheta).containsExactly(expectedTheta);
    }

    private static Stream<Arguments> provideTheta() {
        return Stream.of(
                Arguments.of(theta(0, 0), theta(-11.540747402637631, 7.853890751497814)),
                Arguments.of(theta(1, 1), theta(-11.529445848110777, 7.846369926270659)),
                Arguments.of(theta(2, 3), theta(-11.532294095940571, 7.848265339291818))
        );
    }

    @Test
    public void learnTheta_hypothesisChanges_learnsExpectedTheta() {
        config.withHypothesis((theta, data) -> theta[0] + theta[1] * data[0] * data[0]).build();
        logisticRegression = new LogisticRegression(config.build(), randomProvider);

        double[] newTheta = logisticRegression.learnTheta(prepareDataSet());

        assertThat(newTheta).containsExactly(-7.494676573614041, 3.106741328987135);
    }

    private LogisticRegressionConfig.Builder prepareConfig() {
        return new LogisticRegressionConfig.Builder()
                .withHypothesis((theta, data) -> theta[0] + theta[1] * data[0])
                .withInitialTheta(new double[]{0, 0})
                .withIterations(1000)
                .withLearningRate(1)
                .withDebug(true);
    }

    private DataSet prepareDataSet() {
        return new DataSet(singleValueData(1, 2, 3), results(0, 1, 1));
    }
}
