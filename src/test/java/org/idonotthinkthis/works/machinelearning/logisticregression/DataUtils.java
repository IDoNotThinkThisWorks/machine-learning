package org.idonotthinkthis.works.machinelearning.logisticregression;

public class DataUtils {
    public static double[][] singleValueData(double... values) {
        double[][] result = new double[values.length][];
        for (int i = 0; i< result.length; i++) {
            result[i] = new double[] { values[i] };
        }

        return result;
    }

    public static double[][] complexData(double[]... data) {
        return data;
    }

    public static double[] array(double... values) {
        return values;
    }

    public static int[] results(int... result) {
        return result;
    }

    public static double[] theta(double... values) {
        return values;
    }
}
