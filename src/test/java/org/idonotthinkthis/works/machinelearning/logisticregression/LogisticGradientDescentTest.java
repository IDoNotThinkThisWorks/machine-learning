package org.idonotthinkthis.works.machinelearning.logisticregression;

import org.idonotthinkthis.works.machinelearning.DataSet;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.function.BiFunction;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.idonotthinkthis.works.machinelearning.logisticregression.DataUtils.*;

public class LogisticGradientDescentTest {
    private static final double LEARNING_RATE = 0.1;
    private static final BiFunction<double[], double[], Double> SIMPLE_HYPOTHESIS =
            (theta, data) -> theta[0] + theta[1] * data[0];
    private static final BiFunction<double[], double[], Double> COMPLEX_HYPOTHESIS =
            (theta, data) -> theta[0] + theta[1] * data[0] + theta[2] * data[1] + theta[3] * data[2];

    @ParameterizedTest
    @MethodSource("provideThetaForSingleDataEntry")
    public void newTheta_singleDataEntry_calculatesExpectedTheta(double[] theta, double[] expectedTheta) {
        LogisticGradientDescent gradientDescent = new LogisticGradientDescent(SIMPLE_HYPOTHESIS, LEARNING_RATE);
        DataSet dataSet = new DataSet(singleValueData(1.1), results(0));

        double[] result = gradientDescent.newTheta(theta, dataSet);

        assertThat(result).containsExactly(expectedTheta);
    }

    private static Stream<Arguments> provideThetaForSingleDataEntry() {
        return Stream.of(
                Arguments.of(theta(-1, -1), theta(-1.0109096821195613, -1.0120006503315173)),
                Arguments.of(theta(-0.5, -0.5), theta(-0.5259225100817846, -0.5285147610899631)),
                Arguments.of(theta(0, 0), theta(-0.05, -0.05500000000000001)),
                Arguments.of(theta(0.5, 0.5), theta(0.4259225100817846, 0.41851476108996305)),
                Arguments.of(theta(1, 1), theta(0.9109096821195612, 0.9020006503315174)),
                Arguments.of(theta(5, 5), theta(4.900002753569112, 4.890003028926023))
        );
    }

    @ParameterizedTest
    @MethodSource("provideThetaForComplexHypothesis")
    public void newTheta_complexHypothesis_calculatesExpectedTheta(double[] theta, double[] expectedTheta) {
        LogisticGradientDescent gradientDescent = new LogisticGradientDescent(COMPLEX_HYPOTHESIS, LEARNING_RATE);
        DataSet dataSet = new DataSet(
                complexData(array(-3, -3, -0.1), array(-1.1, -1, -1.5), array(0, 0, 0),
                        array(1.1, 0.5, 1.5), array(4, 3, 4)),
                results(0, 0, 1, 1, 1));

        double[] result = gradientDescent.newTheta(theta, dataSet);

        assertThat(result).containsExactly(expectedTheta);
    }

    private static Stream<Arguments> provideThetaForComplexHypothesis() {
        return Stream.of(
                Arguments.of(theta(-1, -1, -1, -1),
                        theta(-0.9842010368616222, -0.8182437798571885, -0.8519097501405526, -0.8605758387283949)),
                Arguments.of(theta(-0.5, -0.5, -0.5, -0.5),
                        theta(-0.4841494834183122, -0.327764181735441, -0.3599177706630359, -0.36818918477037077)),
                Arguments.of(theta(0, 0, 0, 0),
                        theta(0.01, 0.092, 0.075, 0.071)),
                Arguments.of(theta(0.5, 0.5, 0.5, 0.5),
                        theta(0.5041494834183122, 0.5117641817354409, 0.5099177706630359, 0.5101891847703708)),
                Arguments.of(theta(1, 1, 1.1, 0.8),
                        theta(1.0040405702418806, 1.002564210539058, 1.1021476264120265, 0.80313025500424)),
                Arguments.of(theta(3, 4, 5, 6),
                        theta(3.0009485134859295, 4.000000004648621, 5.000000004164212, 6.000000006337375))
        );
    }
}
