package org.idonotthinkthis.works.machinelearning.logisticregression;

import org.idonotthinkthis.works.machinelearning.utils.CalculationUtils;
import org.idonotthinkthis.works.machinelearning.DataSet;

import java.util.function.BiFunction;

class LogisticGradientDescent {
    private final BiFunction<double[], double[], Double> hypothesis;
    private final double learningRate;

    LogisticGradientDescent(BiFunction<double[], double[], Double> hypothesis, double learningRate) {
        this.hypothesis = hypothesis;
        this.learningRate = learningRate;
    }

    double[] newTheta(double[] theta, DataSet dataSet) {
        double[] calculatedResult = new double[dataSet.getData().length];
        for (int i = 0; i < dataSet.getData().length; i++) {
            double h = hypothesis.apply(theta, dataSet.getData()[i]);
            calculatedResult[i] = CalculationUtils.sigmoid(h) - dataSet.getResults()[i];
        }

        double[] newTheta = new double[theta.length];
        for (int i = 0; i < theta.length; i++) {
            double sum = 0;
            for (int j = 0; j < calculatedResult.length; j++) {
                if (i == 0) {
                    sum += calculatedResult[j];
                } else {
                    sum += calculatedResult[j] * dataSet.getData()[j][i - 1];
                }
            }

            newTheta[i] = theta[i] - (learningRate / calculatedResult.length) * sum;
        }

        return newTheta;
    }
}
