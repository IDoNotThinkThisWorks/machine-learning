package org.idonotthinkthis.works.machinelearning.logisticregression;

import org.idonotthinkthis.works.machinelearning.DataSet;
import org.idonotthinkthis.works.machinelearning.utils.RandomProvider;

import java.util.Arrays;

public class LogisticRegression {
    private final LogisticCostFunction costFunction;
    private final LogisticGradientDescent gradientDescent;
    private final RandomProvider randomProvider;

    private final double[] initialTheta;
    private final int iterations;
    private final boolean debug;

    LogisticRegression(LogisticRegressionConfig config) {
        this(config, new RandomProvider());
    }

    LogisticRegression(LogisticRegressionConfig config, RandomProvider randomProvider) {
        this.gradientDescent = new LogisticGradientDescent(config.getHypothesis(), config.getLearningRate());
        this.costFunction = new LogisticCostFunction(config.getHypothesis());
        this.randomProvider = randomProvider;
        this.initialTheta = config.getInitialTheta();
        this.iterations = config.getIterations();
        this.debug = config.isDebug();
    }

    public double[] learnTheta(DataSet originalDataSet) {
        DataSet dataSet = new DataSet(originalDataSet);
        shuffleData(dataSet);

        double[] theta = Arrays.copyOf(initialTheta, initialTheta.length);
        for (int i = 0; i < iterations; i++) {
            theta = gradientDescent.newTheta(theta, dataSet);

            if (debug) {
                double cost = costFunction.cost(theta, dataSet);
                System.out.println(String.format("%s: Cost: %s | %s", i, cost, Arrays.toString(theta)));
            }
        }

        return theta;
    }

    private void shuffleData(DataSet dataSet) {
        for (int i = dataSet.getData().length - 1; i > 0; i--) {
            int index = randomProvider.nextInt(i + 1);

            double[] tmpData = dataSet.getData()[index];
            dataSet.getData()[index] = dataSet.getData()[i];
            dataSet.getData()[i] = tmpData;

            int tmpResult = dataSet.getResults()[index];
            dataSet.getResults()[index] = dataSet.getResults()[i];
            dataSet.getResults()[i] = tmpResult;
        }
    }
}
