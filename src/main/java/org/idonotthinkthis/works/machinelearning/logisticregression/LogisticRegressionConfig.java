package org.idonotthinkthis.works.machinelearning.logisticregression;

import java.util.function.BiFunction;

public class LogisticRegressionConfig {
    private final BiFunction<double[], double[], Double> hypothesis;
    private final double[] initialTheta;
    private final double learningRate;
    private final int iterations;
    private final boolean debug;

    private LogisticRegressionConfig(BiFunction<double[], double[], Double> hypothesis, double[] theta,
                                    double learningRate, int iterations, boolean debug) {
        this.hypothesis = hypothesis;
        this.initialTheta = theta;
        this.learningRate = learningRate;
        this.iterations = iterations;
        this.debug = debug;
    }

    public BiFunction<double[], double[], Double> getHypothesis() {
        return hypothesis;
    }

    public double[] getInitialTheta() {
        return initialTheta;
    }

    public double getLearningRate() {
        return learningRate;
    }

    public int getIterations() {
        return iterations;
    }

    public boolean isDebug() {
        return debug;
    }

    public static class Builder {
        private BiFunction<double[], double[], Double> hypothesis;
        private double[] initialTheta;
        private double learningRate = 0.1;
        private int iterations = 1000;
        private boolean debug = false;

        public Builder withHypothesis(BiFunction<double[], double[], Double> hypothesis) {
            this.hypothesis = hypothesis;
            return this;
        }

        public Builder withInitialTheta(double[] theta) {
            this.initialTheta = theta;
            return this;
        }

        public Builder withLearningRate(double learningRate) {
            this.learningRate = learningRate;
            return this;
        }

        public Builder withIterations(int iterations) {
            this.iterations = iterations;
            return this;
        }

        public Builder withDebug(boolean debug) {
            this.debug = debug;
            return this;
        }

        public LogisticRegressionConfig build() {
            if (hypothesis == null) {
                throw new IllegalArgumentException("Hypothesis can not be null");
            }

            if (initialTheta == null) {
                throw new IllegalArgumentException("Initial Theta can not be null");
            }

            return new LogisticRegressionConfig(
                    hypothesis,
                    initialTheta,
                    learningRate,
                    iterations,
                    debug);
        }
    }
}
