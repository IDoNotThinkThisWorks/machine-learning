package org.idonotthinkthis.works.machinelearning.logisticregression;

import org.idonotthinkthis.works.machinelearning.utils.CalculationUtils;
import org.idonotthinkthis.works.machinelearning.DataSet;

import java.util.function.BiFunction;

class LogisticCostFunction {
    private final BiFunction<double[], double[], Double> hypothesis;

    LogisticCostFunction(BiFunction<double[], double[], Double> hypothesis) {
        this.hypothesis = hypothesis;
    }

    double cost(double[] theta, DataSet dataSet) {
        double sum = 0;
        for (int i = 0; i< dataSet.getData().length; i++) {
            double h = hypothesis.apply(theta, dataSet.getData()[i]);
            h = CalculationUtils.sigmoid(h);
            if (dataSet.getResults()[i] == 1) {
                sum += Math.log(h);
            } else {
                sum += Math.log(1 - h);
            }
        }

        return (-1.0 / dataSet.getData().length) * sum;
    }
}
