package org.idonotthinkthis.works.machinelearning;

import java.util.Arrays;

public class DataSet {
    private final double[][] data;
    private final int[] results;

    public DataSet(double[][] data, int[] results) {
        this.data = Arrays.copyOf(data, data.length);
        this.results = Arrays.copyOf(results, results.length);
    }

    public DataSet(DataSet copyOf) {
        this.data = Arrays.copyOf(copyOf.data, copyOf.data.length);
        this.results = Arrays.copyOf(copyOf.results, copyOf.results.length);
    }

    public double[][] getData() {
        return data;
    }

    public int[] getResults() {
        return results;
    }
}
