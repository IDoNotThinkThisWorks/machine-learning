package org.idonotthinkthis.works.machinelearning.utils;

public class CalculationUtils {
    public static double sigmoid(double value) {
        return 1 / (1 + Math.exp(-1 * value));
    }
}
