package org.idonotthinkthis.works.machinelearning.utils;

import java.util.Random;

public class RandomProvider {
    private final Random random;

    public RandomProvider() {
        random = new Random();
    }

    public int nextInt(int boundary) {
        return random.nextInt(boundary);
    }
}
